# Recipe Application

Single‐page web application which allows users to manage there favourite recipes.Show all available recipes and the actions to create, update and delete a recipe.

When the user selects a recipe the application Display below attributes on the same screen:
1. Date and time of creation (formatted as dd‐MM‐yyyy HH:mm);
2. Indicator if the dish is vegetarian;
3. Indicator displaying the number of people the dish is suitable for;
4. Display ingredients as a list;
5. Cooking instructions;


##Framework
Framework Technical Specifications :

1. UI - AngularJs 1.5.6
2. Rest API - Java 8, Spring Boot, JPA.
3. API Security - Spring Security
4. API Documentation- Swagger 2.0
5. DB -In Memory database H2
6. CMS- Git

## Rest API Architecture 

All the selection of Technical stack mainly is aimed at two major points 1) Idea to have ease of use, lightweight framework and 2) availability of time and Requirements.
The Spring Framework is a full framework that allows us to create Java enterprise applications. Spring REST easily integrates into other Spring APIs, moreover in Spring Boot 
all we need to worry about is business logic, all boiler plate configuration code of spring is taken care by Spring Boot(all we need to do is to include appropriate starters).  

##Basic functionality:
1. Given: Rest API and UI is up and Running, When click on API URL, then Login Page Should display. 
2. Given: User enter basic auth details correctly, then Dashboard/Screen with recipes list
3. When click on specific recipe, then will show Recipe details as mentioned
 in Sepec.
4. When Recipe details are shown, then option to view and Edit grocery/Ingredient list should be there

## Testing flow
Whenever pushing to live, check that both functionality and UI work and look as expected.
1. Open the project in the browser, see visible recipe in browser in tabular form.
2. .
3. Browse the gallery in hotel page.
4. Make a booking on a pay-at-hotel-property.
5. Check booking details to see that everything is related to the params you selected.
6. Cancel the booking, check the admin to see the booking was indeed cancelled.
7. Make a booking on a prepaid property.
8. Check booking details, countdown, bank details.
9. Go to admin and mark the booking as failed, check booking details. Repeat process with confirmation.
10. Check back button constantly during the navigation.
11. With service workers enabled, make the audit on lighthouse (PWA capabilities must be at 100).
12. With service workers enabled, check the website offline and see if the homepage loads and the autocomplete shows the cities list.



## Tech roadmap

There are some improvements and steps to be taken to scale the API and be able to move faster and better, this are being worked on:

1. End to end testing:

