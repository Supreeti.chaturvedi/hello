package com.tcs.recipe.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.services.ImageService;
import com.tcs.recipe.services.IngredientService;
import com.tcs.recipe.services.RecipeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestAPIControllerTest {

	@Mock
	RecipeService recipeService;
	@Mock
	IngredientService ingredientService;
	@Mock
	ImageService imageService;

	RestAPIController controller;

	Recipe recipe;

	MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		controller = new RestAPIController(recipeService, ingredientService, imageService);
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

		recipe = new Recipe();
		recipe.setId(1L);
		recipe.setTitle("Gajar ka halwa");
		recipe.setDescription("yumm Gajar ka halwa");

	}

	Recipe mockRecipe = new Recipe();

	@Test
	public void testGetRecipe() {

		Set<Recipe> recipes = new HashSet<>();
		recipes.add(recipe);

		Mockito.when(recipeService.getRecipes()).thenReturn(recipes);
		

		
		String s = recipes.stream().map(Object::toString).collect(Collectors.joining(","));

		System.out.println("*******************************     =" + s);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/recipe").accept(
				MediaType.APPLICATION_JSON);
		try {
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expected = "[{\"id\":1,\"dateOfCreation\":null,\"noOfPeopleServing\":null,\"description\":\"yumm Gajar ka halwa\",\"title\":\"Gajar ka halwa\",\"directions\":null,\"ingredients\":[],\"image\":null,\"veg\":false}]";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
		
			mockMvc.perform(get("/api/recipe")).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testShowRecipeById() {
		Mockito.when(recipeService.findById(recipe.getId())).thenReturn(recipe);

		System.out.println("ID " + recipe.getId());
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/recipe/id/" + recipe.getId()).accept(
				MediaType.APPLICATION_JSON);

		try {
			
			MvcResult result = mockMvc.perform(requestBuilder).andReturn();

			System.out.println(result.getResponse().getContentAsString());
			String expected = "{\"id\":1,\"dateOfCreation\":null,\"noOfPeopleServing\":null,\"description\":\"yumm Gajar ka halwa\",\"title\":\"Gajar ka halwa\",\"directions\":null,\"ingredients\":[],\"image\":null,\"veg\":false}";

			JSONAssert.assertEquals(expected, result.getResponse()
					.getContentAsString(), false);
			
			mockMvc.perform(get("/api/recipe/id/" + recipe.getId())).andExpect(status().isOk());
         //   .andExpect(content().json("[{'id': 1,'title': 'Gajar ka halwa';'Description': 'yumm Gajar ka halwa'}]"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testShowRecipeByName() {
		Mockito.when(recipeService.findByName(recipe.getTitle())).thenReturn(recipe);

		System.out.println("Name " + recipe.getTitle());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/recipe/name/" + recipe.getTitle()).accept(
				MediaType.APPLICATION_JSON);

		try {
			
			MvcResult result = mockMvc.perform(requestBuilder).andReturn();

			System.out.println(result.getResponse().getContentAsString());
			String expected = "{\"id\":1,\"dateOfCreation\":null,\"noOfPeopleServing\":null,\"description\":\"yumm Gajar ka halwa\",\"title\":\"Gajar ka halwa\",\"directions\":null,\"ingredients\":[],\"image\":null,\"veg\":false}";

			JSONAssert.assertEquals(expected, result.getResponse()
					.getContentAsString(), false);
			
			mockMvc.perform(get("/api/recipe/name/" + recipe.getTitle())).andExpect(status().isOk());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateRecipe() {
		Mockito.when(recipeService.isRecipeExist(recipe)).thenReturn(false);

		System.out.println("Insert " + recipe.getTitle());

		Mockito.when(recipeService.saveRecipe(any())).thenReturn(recipe);

		try {
			mockMvc.perform(post("api/recipe/").param("description", "some string"))
			/*
			 * .andExpect(status().is3xxRedirection())
			 */;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//.andExpect(status().isOk());

	}

	@Test
	public void testUpdateRecipe() {
	

		Recipe command = new Recipe();
		command.setId(1L);

		when(recipeService.findById(anyLong())).thenReturn(command);

		
		
		command.setTitle(recipe.getTitle());
		command.setDescription(recipe.getDescription());
		
		Mockito.when(recipeService.saveRecipe(command)).thenReturn(command);
		
		try {
			mockMvc.perform(get("/recipe/" +command.getId()).param("description","some string"));
					//.andExpect(status().is2xxRedirection()) ; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDeleteRecipe() {
		
		try {
			mockMvc.perform(get("/recipe/1/delete"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/"));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		verify(recipeService, times(1)).deleteById(anyLong());
	

	}

	
}
