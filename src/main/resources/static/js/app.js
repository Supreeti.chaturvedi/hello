//= require angular
//= require angular-resource
var app = angular.module('app', []);

app.controller('RecipeController', [
		'$scope',
		'RecipeService',
		function($scope, RecipeService) {
			
			$scope.showUpdateSubmit = false;
			$scope.isRecipeUpdatable = false;
			$scope.showRecipes = false;
			$scope.showEditRecipe = false;
			$scope.showCreateRecipe = false;
			$scope.showSingleRecipe = false;
			
			$scope.recipe="";

			$scope.recipe.id="";
			$scope.recipe.title="";
			$scope.recipe.description="";
			$scope.recipe.isVege="";
			$scope.recipe.noOfPeopleServing="";
			$scope.recipe.dateOfCreation="";
			$scope.recipe.directions="";
			$scope.recipe.imagefile="";
			
			$scope.init = function () {
				console.log('init called');
				$scope.showRecipes = true;
				$scope.showEditRecipe = false;
				$scope.showCreateRecipe = false;
				$scope.showSingleRecipe = false;
				$scope.isRecipeUpdatable = false;
				$scope.showUpdateSubmit = false;
				
				$scope.getAllRecipes();
			};

			$scope.updateRecipeShow = function() {
				$scope.recipe.pageHeading="Update Recipe";
				$scope.showEditRecipe = false;
				$scope.isRecipeUpdatable = true;
				$scope.showUpdateSubmit = true;
				$scope.showRecipes = false;
				$scope.showCreateRecipe = false;
				$scope.showSingleRecipe = true;
			}
			
			$scope.showSingleRecipeShow = function() {
				$scope.recipe.pageHeading="Recipe Details";
				$scope.showSingleRecipe = true;
				$scope.showEditRecipe = false;
				$scope.showRecipes = false;
				$scope.showCreateRecipe = false;
				$scope.isRecipeUpdatable = false;
				$scope.showUpdateSubmit = false;
			}
			
			$scope.saveImage = function (form) {
				var id = $scope.recipe.id;
				console.log('saveImage : recipe id is ' + id);
				RecipeService.saveImage($scope.recipe.id, form).then(
						function success(response) {
							$scope.recipe = response.data;
							$scope.recipe.id = id;
							$scope.message = '';
							$scope.errorMessage = '';
						}, function error(response) {
							$scope.message = '';
							if (response.status === 404) {
								$scope.errorMessage = 'Recipe not found!';
							} else {
								$scope.errorMessage = "Error getting recipe!";
							}
						});
			}

			$scope.getRecipe = function(id) {
				//var id = $scope.recipe.id;
				//console.log('recipe id is ' + $scope.recipe.id);
				console.log('recipe id is passed ' + id);
				
				RecipeService.getRecipe(id).then(
						function success(response) {
							$scope.recipe = response.data;
							$scope.recipe.id = id;
							
							console.log("$scope.recipe.id :: " + $scope.recipe.id);
							console.log("$scope.recipe :: " + $scope.recipe);
							console.log("$scope.recipe.isVeg :: " + $scope.recipe.isVeg);
							$scope.message = '';
							$scope.errorMessage = '';
							
							if(id!=null){
								console.log("Showing single recipe");
								$scope.showSingleRecipeShow();
							}
							else{
								console.log("Showing create recipe");
								$scope.createRecipeShow();
							}
							
						}, function error(response) {
							$scope.message = '';
							if (response.status === 404) {
								$scope.errorMessage = 'Recipe not found!';
							} else {
								$scope.errorMessage = "Error getting recipe!";
							}
						});
			}
			
			$scope.updateRecipe = function(id, title) {
				console.log('updateRecipe id is passed ' + id);
				
				RecipeService.updateRecipe(id, title, $scope.recipe.description).then(
						function success(response) {
							console.log("got sucess response ");
							$scope.message = 'Recipe data updated!';
							$scope.errorMessage = '';
				}, function error(response) {
					$scope.errorMessage = 'Error updating Recipe!';
					$scope.message = '';
				}).catch(function(e){
	                console.log("Error: ", e);
	                throw e;
	            });
				
				$scope.getRecipe(id);
			}
			
			$scope.createRecipeShow = function() {
				console.log('createRecipeShow called');
				$scope.recipe.pageHeading="Create New Recipe";
				$scope.showCreateRecipe = true;
				$scope.showEditRecipe = false;
				$scope.showRecipes = false;
				$scope.showSingleRecipe = false;
				$scope.isRecipeUpdatable = false;
				$scope.showUpdateSubmit = false;
			}

			$scope.addRecipe = function() {
				console.log('addRecipe called');
				if ($scope.recipe != null && $scope.recipe.title) {
					RecipeService
							.addRecipe($scope.recipe.title, $scope.recipe.description)
							.then(function success(response) {
								$scope.message = 'Recipe added!';
								$scope.errorMessage = '';
							}, function error(response) {
								$scope.errorMessage = 'Error adding recipe!';
								$scope.message = '';
							});
				} else {
					$scope.errorMessage = 'Please enter a title!';
					$scope.message = '';
				}
			}

			$scope.deleteRecipe = function(id) {
				RecipeService.deleteRecipe(id).then(
						function success(response) {
							$scope.message = 'Recipe deleted!';
							$scope.recipe = null;
							$scope.errorMessage = '';
						}, function error(response) {
							$scope.errorMessage = 'Error deleting recipe!';
							$scope.message = '';
						})
			}

			$scope.getAllRecipes = function() {
				RecipeService.getAllRecipes().then(function success(response) {
					$scope.recipes = response.data;
					
					$scope.message = '';
					$scope.errorMessage = '';
				}, function error(response) {
					$scope.message = '';
					$scope.errorMessage = 'Error getting recipes!';
				});
			}

		} ]);

app.service('RecipeService', [ '$http', function($http) {

	this.getRecipe = function getRecipe(recipeId) {
		return $http({
			method : 'GET',
			url : '/api/recipe/id/' + recipeId
		});
	}
	
	this.saveImage = function getRecipe(recipeId, form) {
		return $http({
			method : 'POST',
			url : '/api/recipe/saveImage/' + recipeId,
			data : {
				id : recipeId,
				file : form.file
			}
		});
	}

	this.addRecipe = function addRecipe(title, description) {
		return $http({
			method : 'POST',
			url : '/api/recipes/',
			data : {
				title : title,
				description : description
			}
		});
	}

	this.deleteRecipe = function deleteRecipe(id) {
		return $http({
			method : 'DELETE',
			url : '/api/recipes/' + id
		})
	}

	this.updateRecipe = function updateRecipe(id, title, description) {
		var urlUpdate = '/api/recipe/' + id;
		
		console.log("update with put called with recipe :: " + id + " :: URLupdate :: " + urlUpdate);
		
		var dataj = {
				title : title,
				description : description
		};
		
		var datajson = JSON.stringify(dataj)
		//return $http.put(urlUpdate, JSON.stringify(data));
		
		return $http({
			method : 'PUT',
			url : urlUpdate,
			data : datajson, 
		
			headers: {
				"Content-Type": "application/json;charset=UTF-8",
				"Access-Control-Allow-Origin": "self",
			    "cache-control": "no-cache",
			},
			credentials: 'include',
			trasformRequest : angular.identity
		})
	}

	this.getAllRecipes = function getAllRecipes() {
		return $http({
			method : 'GET',
			url : '/api/recipe/'
		});
	}

} ]);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);