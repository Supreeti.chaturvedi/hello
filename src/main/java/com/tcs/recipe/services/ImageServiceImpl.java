package com.tcs.recipe.services;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.recipe.controllers.RestAPIController;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.repositories.RecipeRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ImageServiceImpl implements ImageService {
	public static final Logger logger = LoggerFactory.getLogger(ImageServiceImpl.class);

    private final RecipeRepository recipeRepository;

    public ImageServiceImpl( RecipeRepository recipeService) {

        this.recipeRepository = recipeService;
    }

    @Override
    @Transactional
    public void saveImageFile(Long recipeId, MultipartFile file) {

        try {
            Recipe recipe = recipeRepository.findById(recipeId).get();

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            recipe.setImage(byteObjects);

            recipeRepository.save(recipe);
            
            getFileFromMultipartFile(recipe, file);
        } catch (IOException e) {
            //todo handle better
            log.error("Error occurred", e);

            e.printStackTrace();
        }
    }
    
    public File getFileFromMultipartFile(Recipe recipe, MultipartFile multipartFile){
    	String directoryName = ImageServiceImpl.class.getResource("/").getFile();
    	String fileName = recipe.getTitle()+"_"+recipe.getId()+".jpg";
    	
    	String completePath = directoryName+File.pathSeparator+fileName;
    	
    	logger.info("savign the file uploaded by user to location :: " + completePath);
    	
    	File convFile = new File(completePath);
    	
    	try {
			multipartFile.transferTo(convFile);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

    	return convFile;
    }
}
