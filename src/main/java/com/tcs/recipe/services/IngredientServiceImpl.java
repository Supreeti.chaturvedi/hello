package com.tcs.recipe.services;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.recipe.model.Ingredient;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.repositories.RecipeRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IngredientServiceImpl implements IngredientService {

    private final RecipeRepository recipeRepository;

    public IngredientServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Ingredient findByRecipeIdAndIngredientId(Long recipeId, Long ingredientId) {

        Optional<Recipe> recipeOptional = recipeRepository.findById(recipeId);

        if (!recipeOptional.isPresent()){
            //todo impl error handling
            log.error("recipe id not found. Id: " + recipeId);
        }

        Recipe recipe = recipeOptional.get();

        Optional<Ingredient> ingredientOptional = recipe.getIngredients().stream()
                .filter(ingredient -> ingredient.getId().equals(ingredientId)).findFirst();

        if(!ingredientOptional.isPresent()){
            //todo impl error handling
            log.error("Ingredient id not found: " + ingredientId);
        }

        return ingredientOptional.get();
    }
    
    @Override
    public Ingredient findByName(Ingredient ingredient) {
    	if(ingredient.getRecipe() != null) {
    		Optional<Ingredient> ingredientOptional = ingredient.getRecipe().getIngredients().stream().
    		    filter(recipe -> recipe.getName().equals(ingredient.getName())).
    		    findFirst();

    		return ingredientOptional.get();
    	}
    	
    	return null;
    }
    
    @Override
    public Ingredient saveIngredient(Ingredient ingredient, long recipeId) {
    	if(ingredient.getRecipe()==null) {
    		Optional<Recipe> recipeOptional = recipeRepository.findById(recipeId);
    		if(recipeOptional.get()!=null) {
    			Recipe recipe = recipeOptional.get();
    			ingredient.setRecipe(recipe);
    		}
    	}

    	return saveIngredient(ingredient);
    }

    @Override
    @Transactional
    public Ingredient saveIngredient(Ingredient ingredient) {
    	if(ingredient.getRecipe()!=null) {
	        Optional<Recipe> recipeOptional = recipeRepository.findById(ingredient.getRecipe().getId());
	
	        if(!recipeOptional.isPresent()){
	
	            log.error("Recipe not found for id: " + ingredient.getRecipe().getId());
	            return new Ingredient();
	        } else {
	            Recipe recipe = recipeOptional.get();
	
	            log.info("recipe found with ID :: " + recipe.getId());
	            
	            Optional<Ingredient> ingredientOptional = recipe
	                    .getIngredients()
	                    .stream()
	                    .filter(ingredient1 -> ingredient1.getId().equals(ingredient.getId()))
	                    .findFirst();
	
	            if(ingredientOptional.isPresent()){
	                Ingredient ingredientFound = ingredientOptional.get();
	                
	                log.info("ingredient found  :: " + ingredientFound.getId() + " :: recipe Id inside ingredient :: " + ingredientFound.getRecipe().getId());
	                
	                ingredientFound.setDesc(ingredient.getDesc());
	                ingredientFound.setPortion(ingredient.getPortion());
	            } else {
	                Ingredient ingredientTemp = new Ingredient();
	                ingredientTemp.setRecipe(recipe);
	                
	                log.info("ingredient NOT found self created ingredient :: " + ingredientTemp.getId() + " :: recipe Id inside ingredient :: " + ingredientTemp.getRecipe().getId());
	                
	                recipe.addIngredient(ingredientTemp);
	            }
	
	            Recipe savedRecipe = recipeRepository.save(recipe);
	            
	            Optional<Ingredient> savedIngredientOptional = savedRecipe.getIngredients().stream()
	                    .filter(recipeIngredients -> recipeIngredients.getId().equals(ingredient.getId()))
	                    .findFirst();
	
	            if(!savedIngredientOptional.isPresent()){
	                savedIngredientOptional = savedRecipe.getIngredients().stream()
	                        .filter(recipeIngredients -> recipeIngredients.getDesc().equals(ingredient.getDesc()))
	                        .filter(recipeIngredients -> recipeIngredients.getPortion().equals(ingredient.getPortion()))
	                        .findFirst();
	            }
	            
	            Ingredient ingredientToReturn = savedIngredientOptional.get();
	            
	            return ingredientToReturn;
	        }
    	}
    	
    	return new Ingredient();
    }

    @Override
    public void deleteById(Long recipeId, Long idToDelete) {

        log.debug("Deleting ingredient: " + recipeId + ":" + idToDelete);

        Optional<Recipe> recipeOptional = recipeRepository.findById(recipeId);

        if(recipeOptional.isPresent()){
            Recipe recipe = recipeOptional.get();
            log.debug("found recipe");

            Optional<Ingredient> ingredientOptional = recipe
                    .getIngredients()
                    .stream()
                    .filter(ingredient -> ingredient.getId().equals(idToDelete))
                    .findFirst();

            if(ingredientOptional.isPresent()){
                log.debug("found Ingredient");
                Ingredient ingredientToDelete = ingredientOptional.get();
                ingredientToDelete.setRecipe(null);
                recipe.getIngredients().remove(ingredientOptional.get());
                recipeRepository.save(recipe);
            }
        } else {
            log.debug("Recipe Id Not found. Id:" + recipeId);
        }
    }
    
    @Override
    public boolean isIngredientExist(Ingredient ingredient) {
    	return findByName(ingredient) != null;
    }
}