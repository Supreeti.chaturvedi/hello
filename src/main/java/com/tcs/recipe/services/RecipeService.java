package com.tcs.recipe.services;

import java.util.Set;

import com.tcs.recipe.model.Recipe;

public interface RecipeService {
	public Set<Recipe> getRecipes();
	public Recipe findById(Long l);
	public Recipe findByName(String name);
	public Recipe saveRecipe(Recipe recipeBackBean);
	public void deleteById(Long l);
	public boolean isRecipeExist(Recipe recipe);
}