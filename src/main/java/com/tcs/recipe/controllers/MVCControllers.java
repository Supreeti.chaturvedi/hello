package com.tcs.recipe.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.tcs.recipe.model.Ingredient;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.services.ImageService;
import com.tcs.recipe.services.IngredientService;
import com.tcs.recipe.services.RecipeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class MVCControllers {
	private final RecipeService recipeService;
	private final IngredientService ingredientService;
	private final ImageService imageService;

	public MVCControllers(RecipeService recipeService, IngredientService ingredientService, ImageService imageService) {
		super();
		this.recipeService = recipeService;
		this.ingredientService = ingredientService;
		this.imageService = imageService;
	}
	
	@RequestMapping({"/mvcResources/recipe/{id}/show"})
	public String showById(@PathVariable String id, Model model) {
		model.addAttribute("recipe", recipeService.findById(new Long(id))); 
		
		return "mvcResources/recipe/show";
	}
	
	@RequestMapping({"mvcResources/recipe/new"})
	public String newRecipe(Model model) {
		model.addAttribute("recipe", new Recipe()); 
		
		return "mvcResources/recipe/recipeform";
	}
	
	@RequestMapping({"mvcResources/recipe/{id}/update"})
	public String updateRecipe(@PathVariable String id, Model model) {
		model.addAttribute("recipe", recipeService.findById(Long.valueOf(id))); 
		
		return "mvcResources/recipe/recipeform";
	}

	//@RequestMapping(name="recipe", method = RequestMethod.POST)
	@PostMapping
	@RequestMapping("mvcResources/recipe")
	public String saveOrUpdate(@ModelAttribute Recipe recipe) {
		Recipe saved = recipeService.saveRecipe(recipe);
		
		return "redirect:/mvcResources/recipe/" + saved.getId() + "/show";
	}
	
	@GetMapping
	@RequestMapping({"mvcResources/recipe/{id}/delete"})
	public String deleteById(@PathVariable String id) {
		log.info("deleting ID :: " + id);
		
		recipeService.deleteById(Long.valueOf(id));
		
		return "redirect:/";
	}
	
	@GetMapping
	@RequestMapping({"/mvcResources/recipeSearch/"})
	public String searchRecipe(@RequestParam(name = "title") String title, Model model) {
		log.info("searching for recipe name :: " + title);
		Recipe recipe = recipeService.findByName(title);
		
		log.info("recipe found from findByName :: " + recipe);
		
		String returnValue = "";
		
		if(recipe!=null) {
			log.info("recipe not null");
			model.addAttribute("recipe", recipe);
			returnValue = "redirect:/mvcResources/recipe/" + recipe.getId() + "/show";
		}
		else {
			log.info("recipe null");
			model.addAttribute("recipe", recipeService.getRecipes()); 
			returnValue = "redirect:/mvcResources/index";
		}
		
		return returnValue;
	}
	
	
	/**
	 * Ingredient controls
	 * 
	 */
	
	@RequestMapping({"/mvcResources/recipe/{recipeId}/ingredients"})
	public String listIngredients(@PathVariable String recipeId, Model model) {
		log.info("Getting ingredients for Recipe ID :: " + recipeId);
		Recipe recipe = recipeService.findById(Long.valueOf(recipeId));
		
		model.addAttribute("recipe", recipe); 
		
		return "mvcResources/recipe/ingredient/list";	
	}
	
	@RequestMapping("mvcResources/recipe/{recipeId}/ingredient/{id}/show")
    public String showRecipeIngredient(@PathVariable String recipeId,
                                       @PathVariable String id, Model model){
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Long.valueOf(recipeId), Long.valueOf(id)));
        return "mvcResources/recipe/ingredient/show";
    }
	
	@GetMapping("mvcResources/recipe/{recipeId}/ingredient/new")
    public String newRecipe(@PathVariable String recipeId, Model model){

        //make sure we have a good id value
        Recipe recipe = recipeService.findById(Long.valueOf(recipeId));
        //todo raise exception if null

        //need to return back parent id for hidden form property
        Ingredient ingredient = new Ingredient();
        ingredient.setRecipe(recipe);
        model.addAttribute("ingredient", ingredient);

        return "mvcResources/recipe/ingredient/ingredientform";
    }

    @GetMapping("mvcResources/recipe/{recipeId}/ingredient/{id}/update")
    public String updateRecipeIngredient(@PathVariable String recipeId,
                                         @PathVariable String id, Model model){
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Long.valueOf(recipeId), Long.valueOf(id)));

        return "mvcResources/recipe/ingredient/ingredientform";
    }

    @PostMapping("mvcResources/recipe/{recipeId}/ingredient")
    public String saveOrUpdate(@PathVariable String recipeId, @ModelAttribute Ingredient ingredient){
    	log.info("recipe ID being passed :: " + recipeId);
        Ingredient saved = ingredientService.saveIngredient(ingredient, Long.valueOf(recipeId).longValue());

        log.info("saved receipe id:" + recipeId);
        log.info("saved ingredient id:" + saved.getId());

        return "redirect:/mvcResources/recipe/" + recipeId + "/ingredient/" + saved.getId() + "/show";
    }

    @GetMapping("mvcResources/recipe/{recipeId}/ingredient/{id}/delete")
    public String deleteIngredient(@PathVariable String recipeId,
                                   @PathVariable String id){

        log.debug("deleting ingredient id:" + id);
        ingredientService.deleteById(Long.valueOf(recipeId), Long.valueOf(id));

        return "redirect:/mvcResources/recipe/" + recipeId + "/ingredients";
    }
    
    /**
     * Image Controls
     */
    
    @GetMapping("mvcResources/recipe/{id}/image")
    public String showUploadForm(@PathVariable String id, Model model){
        model.addAttribute("recipe", recipeService.findById(Long.valueOf(id)));
        
        log.info("editing image for ID :: " + id);

        return "mvcResources/recipe/imageuploadform";
    }

    @PostMapping("mvcResources/recipe/{id}/image")
    public String handleImagePost(@PathVariable String id, @RequestParam("imagefile") MultipartFile file){

        imageService.saveImageFile(Long.valueOf(id), file);

        return "redirect:/mvcResources/recipe/" + id + "/show";
    }

    @GetMapping("mvcResources/recipe/{id}/recipeimage")
    public void renderImageFromDB(@PathVariable String id, HttpServletResponse response) throws IOException {
        Recipe recipe = recipeService.findById(Long.valueOf(id));

        if (recipe.getImage() != null) {
            byte[] byteArray = new byte[recipe.getImage().length];
            int i = 0;

            for (Byte wrappedByte : recipe.getImage()){
                byteArray[i++] = wrappedByte; //auto unboxing
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }
}