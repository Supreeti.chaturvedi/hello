package com.tcs.recipe.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.recipe.services.RecipeService;

@Controller
public class RootContentController {
	private final RecipeService recipeService;
	
	public RootContentController(RecipeService recipeService) {
		super();
		this.recipeService = recipeService;
	}

	@RequestMapping({"", "/", "/index"})
	public String getLandingPage(Model model) {
		model.addAttribute("recipes", recipeService.getRecipes());
		return "mvcResources/index";
	}
	
	@RequestMapping({"/spa", "/angular"})
	public String getSPALandingPage(Model model) {
		model.addAttribute("recipes", recipeService.getRecipes());
		return "angularIndex";
	}
}
