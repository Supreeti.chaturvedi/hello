package com.tcs.recipe.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="Recipe Object")
@Entity
public class Recipe {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ApiModelProperty(notes="Date Of Creation of recipe")
	private Timestamp dateOfCreation;
	@ApiModelProperty(notes="Number of people the dish is suitable for")
	private Integer noOfPeopleServing;
	@ApiModelProperty(notes="Description of Recipe ")
	private String description;
	@ApiModelProperty(notes="Recipe Title")
	private String title;
	@ApiModelProperty(notes="Indicator for vegetarian recipe")
	private boolean isVeg;
	
	@ApiModelProperty(notes="Directions for Cooking ")
	@Lob
	private String directions;
	
	@ApiModelProperty(notes="Recipe Ingredients as a list; ")
	@OneToMany(cascade=CascadeType.ALL, mappedBy="recipe")
	private Set<Ingredient> ingredients = new HashSet<>();
	
	@Lob
	private Byte[] image;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Timestamp getDateOfCreation() {
		return dateOfCreation;
	}
	
	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	
	public Integer getNoOfPeopleServing() {
		return noOfPeopleServing;
	}
	
	public void setNoOfPeopleServing(Integer noOfPeopleServing) {
		this.noOfPeopleServing = noOfPeopleServing;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desciption) {
		this.description = desciption;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDirections() {
		return directions;
	}

	public void setDirections(String directions) {
		this.directions = directions;
	}

	public Set<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public void setIngredients(Set<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public Byte[] getImage() {
		return image;
	}
	
	public void setImage(Byte[] image) {
		this.image = image;
	}
	
	public Recipe addIngredient(Ingredient ingredient) {
		ingredient.setRecipe(this);
		this.ingredients.add(ingredient);
		return this;
	}

	public boolean isVeg() {
		return isVeg;
	}

	public void setVeg(boolean isVeg) {
		this.isVeg = isVeg;
	}
}