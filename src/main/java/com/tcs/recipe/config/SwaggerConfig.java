package com.tcs.recipe.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {
	
	@Bean
	public Docket productApi() {
		
		return new Docket(DocumentationType.SWAGGER_2)
					.select()
					.apis(RequestHandlerSelectors
					.basePackage("com.tcs.recipe"))
					.paths(regex("/api.*"))
					.build()
					.apiInfo(metaInfo());
	}
	
	
	private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfoBuilder()
        		.title("Recipe Application API")
        		.description("Spring Boot Recipe Application Assignment API Documentation")
        		.contact(new Contact("Supreeti Chaturvedi", "","Sups.chaturvedi@gmail.com"))
        		.license("Apache License Version 2.0")
        		.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
        		.version("1.0.0")
                .build();

        return apiInfo;
    }

}
